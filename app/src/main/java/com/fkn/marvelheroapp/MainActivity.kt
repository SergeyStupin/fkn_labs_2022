package com.fkn.marvelheroapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Column(Modifier.background(color = Color.Black).fillMaxSize()) {
                Image(
                    painter = painterResource(id = R.drawable.marvel),
                    contentDescription = null,
                    Modifier
                        .align(
                            Alignment.CenterHorizontally
                        )
                )
                Text(
                    text = "Choose your hero!",
                    color = Color.White,
                    fontSize = 35.sp,
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .padding(16.dp)
                        .wrapContentSize(Alignment.Center)

                )
                Choose()
            }
        }
    }
}
