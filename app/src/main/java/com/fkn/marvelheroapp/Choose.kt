package com.fkn.marvelheroapp

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import dev.chrisbanes.snapper.ExperimentalSnapperApi
import dev.chrisbanes.snapper.LazyListSnapperLayoutInfo
import dev.chrisbanes.snapper.rememberLazyListSnapperLayoutInfo
import dev.chrisbanes.snapper.rememberSnapperFlingBehavior
import java.io.Console

val heroes = mapOf(
    "Deadpool" to R.drawable.deadpool,
    "Iron Man" to R.drawable.iron_man,
    "Dr. Strange" to R.drawable.dr_strange,
    "Spider Man" to R.drawable.spider_man,
    "Tanos" to R.drawable.tanos
)
@OptIn(ExperimentalSnapperApi::class)
@Composable
fun Choose(){
    val lazyListState = rememberLazyListState()
    val colorState = remember{
        mutableStateOf(Color.Red)
    }

    val layoutInfo: LazyListSnapperLayoutInfo = rememberLazyListSnapperLayoutInfo(lazyListState)
    val indexLazyList =remember{
        mutableStateOf(layoutInfo.currentItem?.index)
    }
    indexLazyList.value=layoutInfo.currentItem?.index
    println(indexLazyList.value)
    when(indexLazyList.value){
        0 -> colorState.value = Color.Red
        1 -> colorState.value = Color.Yellow
        2 -> colorState.value = Color.Green
        3 -> colorState.value = Color.Blue
        4 -> colorState.value = Color.Magenta
    }
    LazyRow(
        state = lazyListState,
        flingBehavior = rememberSnapperFlingBehavior(lazyListState),
        modifier = Modifier.fillMaxWidth().background(color = colorState.value),
        contentPadding = PaddingValues(35.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(50.dp)
    ) {
        for(hero in heroes){
           item(hero.key){
               HeroImageItem(text = hero.key, HeroPainter = painterResource(id = hero.value),modifier = Modifier
                   .width(350.dp)
                   .fillMaxSize()
                   .aspectRatio(3 / 4f))
           }
        }

    }

}